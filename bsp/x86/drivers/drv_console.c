/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2006-09-15     QiuYi        the first version
 */

#include <rtconfig.h>

#ifdef BSP_DRV_CONSOLE

#include <rtthread.h>
#include <rthw.h>
#include <rtdevice.h>

#include <board.h>

struct hw_uart_device
{
    rt_uint32_t com_irqno;
    rt_uint32_t kbd_irqno;
    rt_uint32_t com_occur;
    rt_uint32_t kbd_occur;
};

/*******************************************************************/
/* CRT Register */
/*******************************************************************/
#define MONO_BASE       0x3b4
#define MONO_BUF        0xb0000
#define CGA_BASE        0x3d4
#define CGA_BUF         0xb8000

#define CRT_ROWS        25
#define CRT_COLS        80
#define CRT_SIZE        (CRT_ROWS * CRT_COLS)

static unsigned addr_6845;
static rt_uint16_t *crt_buf;
static rt_int16_t  crt_pos;

extern void init_keyboard();
extern int rt_keyboard_isr(void);
extern rt_bool_t rt_keyboard_getc(char* c);

extern void rt_serial_init(void);
extern char rt_serial_getc(void);
extern void rt_serial_putc(const char c);

void rt_console_putc(int c);

/**
 * @addtogroup QEMU
 */
/*@{*/

/**
 * This function initializes cga
 *
 */
void rt_cga_init(void)
{
    rt_uint16_t volatile *cp;
    rt_uint16_t was;
    rt_uint32_t pos;

    cp = (rt_uint16_t *) (CGA_BUF);
    was = *cp;
    *cp = (rt_uint16_t) 0xA55A;
    if (*cp != 0xA55A)
    {
        cp = (rt_uint16_t *) (MONO_BUF);
        addr_6845 = MONO_BASE;
    }
    else
    {
        *cp = was;
        addr_6845 = CGA_BASE;
    }

    /* Extract cursor location */
    outb(addr_6845, 14);
    pos = inb(addr_6845+1) << 8;
    outb(addr_6845, 15);
    pos |= inb(addr_6845+1);

    crt_buf = (rt_uint16_t *)cp;
    crt_pos = pos;
}

/**
 * This function will write a character to cga
 *
 * @param c the char to write
 */
static void rt_cga_putc(int c)
{
    /* if no attribute given, then use black on white */
    if (!(c & ~0xff)) c |= 0x0700;

    switch (c & 0xff)
    {
    case '\b':
        if (crt_pos > 0)
        {
            crt_pos--;
            crt_buf[crt_pos] = (c&~0xff) | ' ';
        }
        break;
    case '\n':
        crt_pos += CRT_COLS;
        /* cascade  */
    case '\r':
        crt_pos -= (crt_pos % CRT_COLS);
        break;
    case '\t':
        rt_console_putc(' ');
        rt_console_putc(' ');
        rt_console_putc(' ');
        rt_console_putc(' ');
        rt_console_putc(' ');
        break;
    default:
        crt_buf[crt_pos++] = c;     /* write the character */
        break;
    }

    if (crt_pos >= CRT_SIZE)
    {
        rt_int32_t i;
        rt_memcpy(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) << 1);
        for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
            crt_buf[i] = 0x0700 | ' ';
        crt_pos -= CRT_COLS;
    }

    outb(addr_6845, 14);
    outb(addr_6845+1, crt_pos >> 8);
    outb(addr_6845, 15);
    outb(addr_6845+1, crt_pos);
}

/**
 * This function will write a character to serial an cga
 *
 * @param c the char to write
 */
void rt_console_putc(int c)
{
    rt_cga_putc(c);
    rt_serial_putc(c);
}


static rt_err_t uart_configure(struct rt_serial_device *serial, struct serial_configure *cfg)
{
    RT_ASSERT(serial != RT_NULL);
    serial->config = *cfg;
    return RT_EOK;
}

static rt_err_t uart_control(struct rt_serial_device *serial, int cmd, void *arg)
{    
    switch (cmd)
    {
    case RT_DEVICE_CTRL_CLR_INT:
        /* disable rx irq */
        break;

    case RT_DEVICE_CTRL_SET_INT:
        /* enable rx irq */
        break;
    }
    return RT_EOK;
}

static int uart_putc(struct rt_serial_device *serial, char c)
{
    rt_cga_putc(c);
    rt_serial_putc(c);
#ifdef BSP_DRV_UGA
    extern void rt_uga_putc(char ch);
    rt_uga_putc(c);
#endif /* BSP_DRV_UGA */
    return 1;
}

static int uart_getc(struct rt_serial_device *serial)
{
    // rt_kprintf("getc\n");
    RT_ASSERT(serial != RT_NULL);
    struct hw_uart_device *uart = (struct hw_uart_device *)serial->parent.user_data;

    char c = -1;
    rt_bool_t ret = RT_TRUE;

    if (uart->com_occur)
    {
        c = rt_serial_getc();
        uart->com_occur = 0;
    }

    if (uart->kbd_occur)
    {
        ret = rt_keyboard_getc(&c);

        if (ret == RT_FALSE)
        {
            c = -1;
        }
        uart->kbd_occur = 0;
    }
    return c;
}

static const struct rt_uart_ops _uart_ops =
{
    uart_configure,
    uart_control,
    uart_putc,
    uart_getc,
};

static void rt_console_com_isr(int vector, void* param)
{
    struct rt_serial_device *serial = (struct rt_serial_device *)param;

    RT_ASSERT(serial != RT_NULL);
    struct hw_uart_device *uart = (struct hw_uart_device *)serial->parent.user_data;

    uart->com_occur = 1;

    rt_hw_serial_isr(serial, RT_SERIAL_EVENT_RX_IND);
}

static void rt_console_kbd_isr(int vector, void* param)
{
    // rt_kprintf("\n>>> %s\n", __func__);
    
    if (rt_keyboard_isr() == -1)
        return;
    
    struct rt_serial_device *serial = (struct rt_serial_device *)param;
    
    RT_ASSERT(serial != RT_NULL);
    struct hw_uart_device *uart = (struct hw_uart_device *)serial->parent.user_data;

    uart->kbd_occur = 1;

    rt_hw_serial_isr(serial, RT_SERIAL_EVENT_RX_IND);
}

/* UART device driver structure */
static struct hw_uart_device _uart_device =
{
    IRQ4_SERIAL1,
    IRQ1_KEYBOARD,
    0,
    0,
};
static struct rt_serial_device _serial;

/**
 * This function initializes console
 *
 */
int rt_hw_console_init(void)
{
    rt_cga_init();
    rt_serial_init();
    init_keyboard();

    struct hw_uart_device *uart = &_uart_device;
    struct rt_serial_device *serial = &_serial;

    struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;

    serial->ops    = &_uart_ops;
    serial->config = config;

    /* register device */
    rt_hw_serial_register(serial, "console",
                          RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX | RT_DEVICE_FLAG_STREAM,
                          uart);

    /* install keyboard isr */
    rt_hw_interrupt_install(uart->kbd_irqno, rt_console_kbd_isr, serial, "kbd");
    rt_hw_interrupt_umask(uart->kbd_irqno);

    rt_hw_interrupt_install(uart->com_irqno, rt_console_com_isr, serial, "COM1");
    rt_hw_interrupt_umask(uart->com_irqno);

    return 0;
}
// INIT_DEVICE_EXPORT(rt_hw_console_init);

/**
 * This function is used to display a string on console, normally, it's
 * invoked by rt_kprintf
 *
 * @param str the displayed string
 *
 * Modified:
 *  caoxl 2009-10-14
 *  the name is change to rt_hw_console_output in the v0.3.0
 *
 */
void rt_hw_console_output(const char* str)
{
    while (*str)
    {
        rt_console_putc (*str++);
    }
}

#endif /* BSP_DRV_CONSOLE */
