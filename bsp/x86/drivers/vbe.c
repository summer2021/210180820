#include <rtconfig.h>

#ifdef BSP_USING_VBE
#include <rtthread.h>
#include <ioremap.h>
#include <vbe.h>
#include <board.h>

#define VBE_INFO_ADDR  (HW_KERNEL_START + VBE_BASE_INFO_ADDR)
#define VBE_MODE_ADDR  (HW_KERNEL_START + VBE_BASE_MODE_ADDR)

struct vbe_mode_info_block *vbe_mode_info;
struct vbe_info_block *vbe_info;
unsigned char *vbe_base_addr;

void init_vbe(struct multiboot_tag *tag)
{
    struct multiboot_tag_vbe *vbe_tag = (struct multiboot_tag_vbe *)tag;

    struct vbe_info_block *vbe_info = (struct vbe_info_block *)VBE_BASE_INFO_ADDR;
    struct vbe_mode_info_block *mode_info = (struct vbe_mode_info_block *)VBE_BASE_MODE_ADDR;

    rt_memcpy(vbe_info, &(vbe_tag->vbe_control_info), sizeof(struct vbe_info_block));
    rt_memcpy(mode_info, &(vbe_tag->vbe_mode_info), sizeof(struct vbe_mode_info_block));
}

void mutboot2_init_framebuffer(struct multiboot_tag *tag)
{
    struct multiboot_tag_framebuffer *framebuffer_tag = (struct multiboot_tag_framebuffer *)tag;

    struct vbe_mode_info_block *mode_info = (struct vbe_mode_info_block *)VBE_BASE_MODE_ADDR;

    struct vbe_info_block *vbe_info = (struct vbe_info_block *)VBE_BASE_INFO_ADDR;
    if (!vbe_info->vbeVeision)
    {
        vbe_info->vbeVeision = 0x0300;   /* modify as v 0.3.0 */        
    }

    mode_info->xResolution = framebuffer_tag->common.framebuffer_width;
    mode_info->yResolution = framebuffer_tag->common.framebuffer_height;
    mode_info->bitsPerPixel = framebuffer_tag->common.framebuffer_bpp;
    mode_info->phyBasePtr = framebuffer_tag->common.framebuffer_addr;
    mode_info->bytesPerScanLine = framebuffer_tag->common.framebuffer_pitch;
}

int rt_hw_vbe_init(void)
{
    vbe_info = (struct vbe_info_block *)VBE_INFO_ADDR;
    vbe_mode_info = (struct vbe_mode_info_block *)VBE_MODE_ADDR;

    vbe_base_addr = (unsigned char *)rt_ioremap((void *)vbe_mode_info->phyBasePtr, vbe_mode_info->bytesPerScanLine * vbe_mode_info->yResolution);
    
    if (vbe_base_addr == RT_NULL) 
    {
        return -RT_ERROR;
    }
    
    return 0;
}
#endif /* BSP_USING_VBE */