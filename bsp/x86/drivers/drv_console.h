/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-9-21      JasonHu      first version
 */

#ifndef __DRV_CONSOLE_H__
#define __DRV_CONSOLE_H__

int rt_hw_console_init(void);

#endif  /* __DRV_CONSOLE_H__ */
