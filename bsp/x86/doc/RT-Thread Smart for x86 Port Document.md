# RT-Thread Smart for x86 Port Document

作者：胡自成

日期：2021.9.30

 RT-Thread Smart是一个支持进程的`RTOS`，可以支持`posix`应用程序，也支持多平台。这是对`x86`平台的移植的一份文档描述。

本文档主要分为4个部分进行描述。分别是，`libcpu，bsp，lwp/arch，musllibc i386`的描述。

## 一、libcpu部分

​	这部分是存放和`CPU`相关的代码，上下文切换，中断，`mmu`等内容是位于这里面。

1. 引导部分

   `x86`采用的是`grub2`引导，只需要按照`grub2`的规范来写入口程序，就可以正常进入保护模式，可以大大减少工作量。在`start_gcc.S`中是`multiboot2`的协议，`grub2`初始化后就会进入`_start`，然后跳转到`multiboot_entry`执行，做了简单的初始化后，就进入`primary_cpu_entry`，然后就会调用`rt_hw_board_init`做初始化。

2. `MMU`部分

   在进入board后，会调用初始化`MMU`，打开分页模式。目前做法是将内核空间的物理地址和虚拟地址一一映射，映射后就可以直接访问物理地址了。`x86`采用的是`4KB`的物理页，采用3级页表，即页目录表，页表，物理页，物理页是存放数据的页，都是`4KB`。`MMU`主要是适配几个接口，这几个接口主要是做页表映射和解除页表映射，用户空间范围初始化等，分别是：

   ```c
   rt_hw_mmu_map_init
   rt_hw_mmu_map
   rt_hw_mmu_map_auto
   rt_hw_mmu_unmap
   rt_hw_mmu_v2p
   ```

   

3. `cpu`相关的机制

   这里就得提到一些`x86`的老东西了，`GDT,IDT,TSS`等。`GDT`主要是初始化段，初始化后就可以正常执行代码和读写数据了。`IDT`是初始化中断表，往里面填写中断处理程序的入口地址就可以捕捉中断了，`TSS`是任务状态段，主要用于保存任务的内核栈的地址。

4. 中断管理

   中断是由`IDT`这个表来记录，`IDTR`来保存这个表在内存中的位置。除此之外，还需要初始化`PIC`中断控制器，用它来对中断进行屏蔽和接触屏蔽，以及对一个中断处理完后进行应答。最后将会实现一下接口：

   ```c
   rt_hw_interrupt_install
   rt_hw_interrupt_umask
   rt_hw_interrupt_mask
   ```

   除此之外，还支持对异常的处理，比如页故障的处理。

5. 上下文切换

   上下文切换的原理就是保存当前的寄存器到`from`栈中，然后保存`from`栈的值。然后获取`to`线程的栈，从栈中恢复寄存器。

   首先需要看的`rt_hw_stack_init`，这是初始化线程栈的内容，填写入口程序，参数，返回地址等。

   然后再看`rt_hw_context_switch_to`，这是当第一个线程执行的时候，就会调用它，直接从栈中恢复寄存器。这里面需要调用`rt_hw_tss_set_kstacktop`设置当前线程的内核栈顶，当从用户态陷入内核的时候就会使用这个栈顶。也就是说每个进程有2个栈，一个内核栈，一个用户栈。

   然后就是切换到线程的`MMU`，使得页表生效，就可以访问进程的虚拟地址了。

   最后就是切换寄存器上下文，从`to`线程中恢复寄存器。此时会使用`to`中指定的栈顶，`eip`寄存器就是`PC`寄存器，里面就是我们初始化的入口程序处。

   不过值得注意的是，最开始的入口并不是用户指定的入口，而是`rt_hw_thread_entry`这个函数，在这个函数里面再调用用户的函数，这样的目的是为了当线程返回的时候，可以比较方便处理后事。

   `rt_hw_context_switch`这个函数会先调用`lwp_user_setting_save`，保存当前线程的设置，然后设置内核栈顶以及切换`MMU`，最后再调用`rt_hw_context_switch_real`来做寄存器上下文切换。当线程切换回来的时候，再调用`lwp_user_setting_restore`恢复之前的设置。

   `rt_hw_context_switch_interrupt`只是设置中断标志，如果有中断产生，就设置标志，当中断退出的时候才会去调用调度函数。如果标志设置，则会调用`rt_hw_intr_thread_switch`来进行上下文切换，最终调用`rt_hw_context_switch`来完成这个工作。

## 二、BSP部分

​	`BSP`里面就是做一些驱动的初始化的内容。核心是围绕`rt_hw_board_init`展开，主要的工作如下：

1. 初始化串口调试

2. 初始化`MMU`
3. 初始化`x86`架构特殊的内容
4. 初始化中断
5. 初始化串口驱动
6. 初始化控制台驱动
7. 设置控制台的输出设备
8. 初始化定时器驱动
9. 初始化`DMA`内存池
10. 初始化`PCI`总线
11. 初始化组件

在执行完成一系列的初始化后，`BSP`的工作就完成了！然后就是进入内核真正执行了。

## 三、LWP/ARCH

这部分代码，主要是做的是和进程相关的内容。代码位于`components/lwp/arch/i386`目录下面。

`arch_user_space_init`是用于初始化进程的用户态的页表的内容。

`arch_kernel_mmu_table_get`是获取内核页表。

`rt_cpu_set_thread_idr`和`rt_cpu_get_thread_idr`是用于设置`TLS`用的，就是线程本地储存的东西，每个线程都需要有一个这个的东西。由于没有寄存器可以用，所以，这里使用了`GS`段，通过这个段映射一个地址，然后通过`GS`访问0地址就可以获取到`TLS`的内容。

`lwp_user_thread_entry`是用户态线程的入口函数，是`sys_thread_create`来调用的。里面需要在用户空间的一个栈中构建线程的上下文，然后再这个上下文中恢复执行。这个就是用户线程的地层实现。

`lwp_user_entry`是用户的执行入口，就是当第一次执行的时候，就需要从这个地方进入用户态执行。

`lwp_exec_user`就是执行用户程序，它调用了`lwp_user_entry`。

`lwp_switch_to_user`是内核态切换到用户态的代码，从栈中恢复寄存器，使用用户的段选择子。使用用户态的特权级，就可以执行用户程序了。

`lwp_set_thread_context`是用来设置线程的寄存器上下文的，`fork`和`clone`就需要这个上下文来运行。

`lwp_try_do_signal`是用来执行信号捕捉的，也就是通过检查信号，如果有，则进入用户态执行捕捉函数。首先会记录当前的中断`frame`，然后生成一个信号的`frame`，返回的时候就从这个新的`frame`中返回，就会去执行用户态的信号捕捉函数。等捕捉函数执行完后，就会调用信号退出系统调用，来调用`lwp_signal_do_return`，然后会恢复之前的中断上下文。

## 四、musllibc架构相关代码

用户程序需要使用c库来作为基础，在执行`main`函数之前，还有`libc`需要执行很多初始化的内容，在此，也会有一些不同架构有差异的地方，我们需要来做适配。

1. `crt`的适配

   在`musl/arch/i386/crt_arch.h`中修改入口参数的支持。

   ```assembly
   /* rtt-smart version */
   __asm__(
   ".text\n"
   ".weak _DYNAMIC \n"
   ".hidden _DYNAMIC \n"
   ".global " START "\n"
   START ":\n"
   "   xor %ebp,%ebp \n"
   "   mov %esp,%eax \n"
   "   and $-16,%esp \n"
   "   push %eax \n"
   "   push %eax \n"
   "   call 1f \n"
   "1: addl $_DYNAMIC-1b,(%esp) \n"
   "   push %ebx \n"
   "   call " START "_c \n"
   );
   ```

   `esp`是用户栈顶，`ebx`是参数地址，把它压入栈，那么就可以在函数_start_c中获取参数，解析出我们需要的`argc，argv，envp`等重要的数据。

   在`musl/crt/crt1.c`中有`_start_c`，传入一个参数`p`，它是一个指针，指向了一个地址，这个地址中有`argc，argv等`内容。

   ```
   #include <features.h>
   #include "libc.h"
   
   #define START "_start"
   
   #include "crt_arch.h"
   
   int main();
   weak void _init();
   weak void _fini();
   _Noreturn int __libc_start_main(int (*)(), int, char **,
   	void (*)(), void(*)(), void(*)());
   
   void _start_c(long *p)
   {
   	int argc = p[0];
   	char **argv = (void *)(p+1);
   	__libc_start_main(main, argc, argv, _init, _fini, 0);
   }
   ```

   

2. 系统调用

   `rt-smart`的系统调用是对原来的`musl`库做了一定修改，做了一个底层的转接，就是把`Linux`的系统调用号转换成了`rt-smart`的系统调用号，然后再调用。代码在`musl/src/internal/i386/rtt_syscall.h`中。

   ```assembly
   .text
   /*
    * Convert the musl system call numbers to RT-Thread ones.
    *
    * Procedure: __syscall -> ___syscall() -> get_rtt_syscall() -> syscall
    */
   .global __syscall
   .type __syscall, %function
   __syscall:
       jmp   ___syscall
   
   .hidden __sysinfo
   
   # The calling convention for __vsyscall has the syscall number
   # and 5 args arriving as:  eax, edx, ecx, edi, esi, 4(%esp).
   # This ensures that the inline asm in the C code never has to touch
   # ebx or ebp (which are unavailable in PIC and frame-pointer-using
   # code, respectively), and optimizes for size/simplicity in the caller.
   
   .global __vsyscall
   .hidden __vsyscall
   .type __vsyscall,@function
   __vsyscall:
       push %edi
       push %ebx
       mov %edx,%ebx
       mov %edi,%edx
       mov 12(%esp),%edi
       push %eax
       call 1f
   2:  mov %ebx,%edx
       pop %ebx
       pop %ebx
       pop %edi
       ret
   
   1:  mov (%esp),%eax
       add $[__sysinfo-2b],%eax
       mov (%eax),%eax
       test %eax,%eax
       jz 1f
       push %eax
       mov 8(%esp),%eax
       ret                     # tail call to kernel vsyscall entry
   1:  mov 4(%esp),%eax
       int $0x80
       ret
   
   # The __vsyscall6 entry point is used only for 6-argument syscalls.
   # Instead of passing the 5th argument on the stack, a pointer to the
   # 5th and 6th arguments is passed. This is ugly, but there are no
   # register constraints the inline asm could use that would make it
   # possible to pass two arguments on the stack.
   
   .global __vsyscall6
   .hidden __vsyscall6
   .type __vsyscall6,@function
   __vsyscall6:
       push %ebp
       push %eax
       mov 12(%esp), %ebp
       mov (%ebp), %eax
       mov 4(%ebp), %ebp
       push %eax
       mov 4(%esp),%eax
       call __vsyscall
       pop %ebp
       pop %ebp
       pop %ebp
       ret
   
   /* Invoke the RT-Thread system call interfaces. */
   .global syscall
   .type syscall, %function
   syscall:
       lea 24(%esp),%eax
       push %esi
       push %edi
       push %eax
       mov 16(%esp),%eax
       mov 20(%esp),%edx
       mov 24(%esp),%ecx
       mov 28(%esp),%edi
       mov 32(%esp),%esi
       call __vsyscall6
       pop %edi
       pop %edi
       pop %esi
       ret
   
   ```

   转换流程如下，最后会转换到`syscall`来调用`rt-smart`的系统调用。

   ```
   __syscall -> ___syscall() -> get_rtt_syscall() -> syscall
   ```

   `rt-smart`的系统调用号为`x86`，和`Linux`一致。

   在`musl/src/internal/syscall.h`中对`__syscall`进行了封装，使得可以把系统调用号路由到rt-smart的系统调用号中去。

   当调用`___syscall`时，就会嗲用`get_rtt_syscall`做转换，然后再调用具体的系统调用。

   `__syscall`又是面向系统调用的直接接口，所以它会调用 `___syscall`.因此这条线就连起来了。

   也就是当使用了`___syscall`，就会路由到rtt的系统调用，如果使用了`___syscall#N`就会直接调用syscall进行系统调用，不路由。

3.  `syscall_set_thread_area`

   这个函数是要设置`TLS`的地址，这样每个线程就能够在设置和访问一个私有的数据区域。

   它是在`musl/src/thread/i386/__set_thread_area.c`中的，它就是调用一个系统调用，于是这里直接通过`__syscall`来路由到`rtt`的系统调用直接调用内核的函数。这个地方一定要是实现，不然，内核就会在读取`tls`内容的时候出现不能读取或者读不到正确的数据的问题。

   ```c
   #include "pthread_impl.h"
   
   int __set_thread_area(void *p)
   {
       return __syscall(0xf0005, p); /* special syscall nr */
   }
   ```

## 五、总结

​	通过对x86架构的移植，这个过程中踩了很多坑，因为资料比较少的情况下，做移植是非常痛苦的，可以说是什么坑都得自己踩。

​	本次移植，从引导到执行用户程序，整个过程都是比较艰辛的，但是也学到了很多新的知识，长了很多经验。

​	整体来说，系统架构还算不错，也是比较容易去填接口。但是如果移植文档多一点就会比较好了！

