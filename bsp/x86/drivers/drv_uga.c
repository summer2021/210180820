#include <rtconfig.h>
#include <rtthread.h>

#ifdef BSP_DRV_UGA
#include <vbe.h>
#include "uga_8x16.h"

static struct
{
    unsigned short x_sz, y_sz;
    unsigned short row, col;
    unsigned int fill, clear;
    unsigned char bpp;
    void (*out_pixel)(int, int, uint32_t);
} uga;

#define UGA_FONT_W   8
#define UGA_FONT_H   16
#define UGA_CUR_CODE 219

#define UGA_DBG_Y    20

#define UGA_ARGB_SUB(a, r, g, b) (((a) << 24) | ((r) << 16) | ((g) << 8) | (b)) 
#define UGA_ARGB(a, r, g, b)     UGA_ARGB_SUB((a) & 0xff, (r)  & 0xff, (g) & 0xff, (b) & 0xff)
#define UGA_RGB(r, g, b)         UGA_ARGB(255, r, g, b)

#define UGA_RED        UGA_RGB(255, 0, 0)
#define UGA_GREEN      UGA_RGB(0, 255, 0)
#define UGA_BLUE       UGA_RGB(0, 0, 255)
#define UGA_WHITE      UGA_RGB(255, 255, 255)
#define UGA_BLACK      UGA_RGB(0, 0, 0)
#define UGA_GRAY       UGA_RGB(195, 195, 195)
#define UGA_LEAD       UGA_RGB(127, 127, 127)
#define UGA_YELLOW     UGA_RGB(255, 255, 0)
#define UGA_NONE       UGA_ARGB(0, 0, 0, 0)

#define UGA_COLOR_DEFAULT  UGA_GREEN

static unsigned short SCREEN_WIDTH;
static unsigned short SCREEN_HEIGHT;

static void screen_out_pixel16(int x, int y, uint32_t color)
{
    uint32_t  r, g, b;
    b = color&0xF8;
    g = color&0xFC00;
    r = color&0xF80000;
    *((short*)((vbe_base_addr) + 2*((SCREEN_WIDTH)*y+x))) = (short)((b>>3)|(g>>5)|(r>>8));
}

static void screen_out_pixel24(int x, int y, uint32_t color)
{
    *((vbe_base_addr) + 3*((SCREEN_WIDTH) * y + x) + 0) = color & 0xFF;
    *((vbe_base_addr) + 3*((SCREEN_WIDTH) * y + x) + 1) = (color & 0xFF00) >> 8;
    *((vbe_base_addr) + 3*((SCREEN_WIDTH) * y + x) + 2) = (color & 0xFF0000) >> 16;
}

static void screen_out_pixel32(int x, int y, uint32_t color)
{
    *((unsigned int*)((vbe_base_addr) + 4 * ((SCREEN_WIDTH) * y + x))) = (unsigned int)color;
}

static void uga_outchar(unsigned short x, unsigned short y, unsigned char ch)
{
    if (uga.out_pixel == RT_NULL)
    {
        return;            
    }

    unsigned int fx = x * UGA_FONT_W;
    unsigned int fy = y * UGA_FONT_H;
    unsigned int fex = fx + UGA_FONT_W;
    unsigned int fey = fy + UGA_FONT_H;
    unsigned int fi = ch * UGA_FONT_H;

    for (; fy < fey; ++fy, ++fi)
    {
        fy *= UGA_FONT_W;
        for (; fx < fex; ++fx)
        {
            if (uga_8x16[fi] >> (fex - fx) & 1)
            {
                uga.out_pixel(fx, fy, uga.fill);
            }
            else
            {
                uga.out_pixel(fx, fy, uga.clear);
            }
        }
        fy /= UGA_FONT_W;
        fx -= UGA_FONT_W;
    }
}

static void uag_scroll(int clear_cursor)
{
    int x, y;
    uint32_t *src, *dst;
    uint32_t byte = uga.bpp / 8;

    if (clear_cursor != 0)
    {
        uga_outchar(uga.col, uga.row, ' ');
    }
    
    for (y = 0; y < (SCREEN_HEIGHT - 1) * UGA_FONT_H; ++y)
    {
        src = (uint32_t *)(vbe_base_addr + ((y + UGA_FONT_H) * (SCREEN_WIDTH * UGA_FONT_W)) * byte);
        dst = (uint32_t *)(vbe_base_addr + (y * (SCREEN_WIDTH * UGA_FONT_W)) * byte);
        for (x = 0; x < (SCREEN_WIDTH * UGA_FONT_W) * byte / 4; ++x)
        {
            dst[x] = src[x];
        }
    }

    for (y = (SCREEN_HEIGHT - 1) * UGA_FONT_H; y < SCREEN_HEIGHT * UGA_FONT_H; ++y)
    {
        dst = (uint32_t *)(vbe_base_addr + (y * (SCREEN_WIDTH * UGA_FONT_W)) * byte);
        for (x = 0; x < (SCREEN_WIDTH * UGA_FONT_W) * byte / 4; ++x)
        {
            dst[x] = uga.clear;
        }
    }
}

void rt_uga_putc(char ch)
{
    switch (ch)
    {
    case '\n':
        if (uga.row < SCREEN_HEIGHT - 1)
        {
            uga_outchar(uga.col, uga.row, ' ');
            ++uga.row;
        }
        else
        {
            uag_scroll(1);
        }
        uga.col = 0;
        break;
    case '\b':
        if (uga.col > 0)
        {
            uga_outchar(uga.col, uga.row, ' ');
            --uga.col;
        }
        break;
    case '\r':
        return;
    default:
        uga_outchar(uga.col, uga.row, ch);
        if (uga.col >= SCREEN_WIDTH - 1)
        {
            if (uga.row >= SCREEN_HEIGHT - 1)
            {
                uag_scroll(0);
            }
            else
            {
                ++uga.row;
            }
            uga.col = 0;
        }
        else
        {
            ++uga.col;
        }
        break;
    }
    uga_outchar(uga.col, uga.row, UGA_CUR_CODE);
}

int rt_hw_uga_init(void)
{
    uga.x_sz  = vbe_mode_info->xResolution;
    uga.y_sz  = vbe_mode_info->yResolution;
    uga.fill  = UGA_WHITE;
    uga.clear = UGA_NONE;
    uga.bpp   = vbe_mode_info->bitsPerPixel;
    uga.row   = 0;
    uga.col   = 0;

    switch (uga.bpp)
    {
        case 16:
            uga.out_pixel = screen_out_pixel16;
            break;
        case 24:
            uga.out_pixel = screen_out_pixel24;
            break;
        case 32:
            uga.out_pixel = screen_out_pixel32;
            break;
        default:
            uga.out_pixel = RT_NULL;
            return -RT_ERROR;
    }

    SCREEN_WIDTH = uga.x_sz / UGA_FONT_W;
    SCREEN_HEIGHT = uga.y_sz / UGA_FONT_H;

    return 0;
}
#endif /* BSP_DRV_UGA */